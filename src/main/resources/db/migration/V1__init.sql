-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema membro
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema membro
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `membro` DEFAULT CHARACTER SET utf8 ;
USE `membro` ;

-- -----------------------------------------------------
-- Table `membro`.`pessoa`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `membro`.`pessoa` (
  `idpessoa` BIGINT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(100) NOT NULL,
  `dob` DATE NOT NULL,
  `cpf` VARCHAR(15) NULL,
  `data_cadastro` DATE NOT NULL,
  `genero` CHAR(1) NOT NULL COMMENT 'M-MASCULINO\nF-FEMININO',
  `status_civil` CHAR(10) NULL,
  `foto_dir` VARCHAR(255) NULL,
  PRIMARY KEY (`idpessoa`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `membro`.`endereco`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `membro`.`endereco` (
  `idendereco` BIGINT NOT NULL AUTO_INCREMENT,
  `cidade` VARCHAR(100) NOT NULL,
  `pais` VARCHAR(50) NOT NULL,
  `estado` VARCHAR(50) NOT NULL,
  `bairro` VARCHAR(100) NULL,
  `cep` VARCHAR(10) NULL,
  `rua` VARCHAR(100) NULL,
  `numero` INT NULL,
  `pessoa_idpessoa` BIGINT NULL,
  PRIMARY KEY (`idendereco`),
  INDEX `fk_endereco_pessoa_idx` (`pessoa_idpessoa` ASC),
  CONSTRAINT `fk_endereco_pessoa`
    FOREIGN KEY (`pessoa_idpessoa`)
    REFERENCES `membro`.`pessoa` (`idpessoa`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `membro`.`telefone`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `membro`.`telefone` (
  `idtelefone` BIGINT NOT NULL AUTO_INCREMENT,
  `telefone` VARCHAR(20) NOT NULL,
  `pessoa_idpessoa` BIGINT NOT NULL,
  PRIMARY KEY (`idtelefone`),
  INDEX `fk_telefone_pessoa1_idx` (`pessoa_idpessoa` ASC),
  CONSTRAINT `fk_telefone_pessoa1`
    FOREIGN KEY (`pessoa_idpessoa`)
    REFERENCES `membro`.`pessoa` (`idpessoa`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `membro`.`email`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `membro`.`email` (
  `idemail` BIGINT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(100) NOT NULL,
  `pessoa_idpessoa` BIGINT NOT NULL,
  PRIMARY KEY (`idemail`),
  INDEX `fk_email_pessoa1_idx` (`pessoa_idpessoa` ASC),
  CONSTRAINT `fk_email_pessoa1`
    FOREIGN KEY (`pessoa_idpessoa`)
    REFERENCES `membro`.`pessoa` (`idpessoa`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `membro`.`igreja`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `membro`.`igreja` (
  `idigreja` BIGINT NOT NULL,
  `razao_social` VARCHAR(100) NOT NULL,
  `fantasia` VARCHAR(100) NOT NULL,
  `cnpj` VARCHAR(20) NOT NULL,
  `endereco_idendereco` BIGINT NOT NULL,
  `matriz_id` BIGINT NULL,
  PRIMARY KEY (`idigreja`),
  INDEX `fk_igreja_endereco1_idx` (`endereco_idendereco` ASC),
  INDEX `fk_igreja_igreja1_idx` (`matriz_id` ASC),
  CONSTRAINT `fk_igreja_endereco1`
    FOREIGN KEY (`endereco_idendereco`)
    REFERENCES `membro`.`endereco` (`idendereco`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_igreja_igreja1`
    FOREIGN KEY (`matriz_id`)
    REFERENCES `membro`.`igreja` (`idigreja`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `membro`.`membro`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `membro`.`membro` (
  `idmembro` BIGINT NOT NULL AUTO_INCREMENT,
  `pessoa_idpessoa` BIGINT NOT NULL,
  `igreja_idigreja` BIGINT NOT NULL,
  `tipo` CHAR(10) NOT NULL COMMENT 'MEMBRO,\nPASTOR,\nETC',
  PRIMARY KEY (`idmembro`),
  INDEX `fk_membro_pessoa1_idx` (`pessoa_idpessoa` ASC),
  INDEX `fk_membro_igreja1_idx` (`igreja_idigreja` ASC),
  CONSTRAINT `fk_membro_pessoa1`
    FOREIGN KEY (`pessoa_idpessoa`)
    REFERENCES `membro`.`pessoa` (`idpessoa`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_membro_igreja1`
    FOREIGN KEY (`igreja_idigreja`)
    REFERENCES `membro`.`igreja` (`idigreja`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
