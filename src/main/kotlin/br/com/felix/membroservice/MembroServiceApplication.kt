package br.com.felix.membroservice

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.netflix.eureka.EnableEurekaClient
import org.springframework.scheduling.annotation.EnableScheduling

@EnableScheduling
@EnableEurekaClient
@SpringBootApplication
class MembroServiceApplication

fun main(args: Array<String>) {
	runApplication<MembroServiceApplication>(*args)
}

