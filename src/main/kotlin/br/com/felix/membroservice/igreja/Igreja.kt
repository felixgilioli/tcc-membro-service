package br.com.felix.membroservice.igreja

import br.com.felix.membroservice.endereco.Endereco
import org.hibernate.validator.constraints.Length
import org.hibernate.validator.constraints.br.CNPJ
import javax.persistence.*

@Entity
data class Igreja(

        @Length(min = 2, max = 100,  message = "razaoSocial deve estar entre 2 e 100 caracteres.")
        @Column(name = "razao_social", length = 100, nullable = false)
        val razaoSocial: String,

        @Length(min = 2, max = 100,  message = "fantasia deve estar entre 2 e 100 caracteres.")
        @Column(length = 100, nullable = false)
        val fantasia: String,

        @CNPJ(message = "cnpj invalido.")
        @Column(length = 20, nullable = false)
        val cnpj: String,

        @OneToOne
        @JoinColumn(name = "endereco_idendereco", nullable = false)
        val endereco: Endereco,

        @ManyToOne
        @JoinColumn(name = "matriz_id")
        val matriz: Igreja? = null,

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "idigreja")
        val idIgreja: Long? = null
)