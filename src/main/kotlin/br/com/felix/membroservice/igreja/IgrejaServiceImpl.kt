package br.com.felix.membroservice.igreja

import br.com.felix.membroservice.core.crud.CrudServiceImpl
import org.springframework.stereotype.Service

@Service
class IgrejaServiceImpl(private val repository: IgrejaRepository) : IgrejaService, CrudServiceImpl<Igreja, Long>() {

    override fun getRepository() = repository
}