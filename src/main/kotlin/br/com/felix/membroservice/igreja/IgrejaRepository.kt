package br.com.felix.membroservice.igreja

import org.springframework.data.jpa.repository.JpaRepository

interface IgrejaRepository : JpaRepository<Igreja, Long> {
}