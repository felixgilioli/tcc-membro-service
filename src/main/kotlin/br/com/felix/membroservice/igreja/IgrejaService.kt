package br.com.felix.membroservice.igreja

import br.com.felix.membroservice.core.crud.CrudService

interface IgrejaService : CrudService<Igreja, Long> {
}