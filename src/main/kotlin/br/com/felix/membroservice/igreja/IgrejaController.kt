package br.com.felix.membroservice.igreja

import br.com.felix.membroservice.core.crud.CrudController
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("igreja")
class IgrejaController(private val service: IgrejaService) : CrudController<Igreja, Long>() {

    override fun getService() = service
}