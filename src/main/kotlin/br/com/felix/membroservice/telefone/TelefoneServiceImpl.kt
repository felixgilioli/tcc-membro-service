package br.com.felix.membroservice.telefone

import br.com.felix.membroservice.core.crud.CrudServiceImpl
import org.springframework.stereotype.Service

@Service
class TelefoneServiceImpl(private val repository: TelefoneRepository) : TelefoneService, CrudServiceImpl<Telefone, Long>() {

    override fun getRepository() = repository
}