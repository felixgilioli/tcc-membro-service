package br.com.felix.membroservice.telefone

import br.com.felix.membroservice.pessoa.Pessoa
import org.hibernate.validator.constraints.Length
import javax.persistence.*

@Entity
data class Telefone(

        @Length(min = 4, max = 20,  message = "telefone deve estar entre 4 e 20 caracteres.")
        @Column(length = 20, nullable = false)
        val telefone: String,

        @ManyToOne(optional = false)
        @JoinColumn(name = "pessoa_idpessoa", nullable = false)
        val pessoa: Pessoa,

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "idtelefone")
        val idTelefone: Long? = null
)