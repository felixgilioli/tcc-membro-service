package br.com.felix.membroservice.telefone

import br.com.felix.membroservice.core.crud.CrudService

interface TelefoneService : CrudService<Telefone, Long> {
}