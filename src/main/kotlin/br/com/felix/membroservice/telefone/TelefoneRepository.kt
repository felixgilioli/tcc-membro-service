package br.com.felix.membroservice.telefone

import org.springframework.data.jpa.repository.JpaRepository

interface TelefoneRepository : JpaRepository<Telefone, Long> {
}