package br.com.felix.membroservice.membro

import br.com.felix.membroservice.core.crud.CrudServiceImpl
import org.springframework.stereotype.Service

@Service
class MembroServiceImpl(private val repository: MembroRepository) : MembroService, CrudServiceImpl<Membro, Long>() {

    override fun getRepository() = repository

}