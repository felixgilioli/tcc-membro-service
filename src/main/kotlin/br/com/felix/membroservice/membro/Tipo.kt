package br.com.felix.membroservice.membro

enum class Tipo {
    MEMBRO,
    OBREIRO,
    PASTOR
}