package br.com.felix.membroservice.membro

import org.springframework.data.jpa.repository.JpaRepository
import java.time.LocalDate

interface MembroRepository : JpaRepository<Membro, Long> {
}