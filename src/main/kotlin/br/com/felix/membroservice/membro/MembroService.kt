package br.com.felix.membroservice.membro

import br.com.felix.membroservice.core.crud.CrudService

interface MembroService : CrudService<Membro, Long> {
}