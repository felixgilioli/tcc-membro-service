package br.com.felix.membroservice.membro

import br.com.felix.membroservice.igreja.Igreja
import br.com.felix.membroservice.pessoa.Pessoa
import javax.persistence.*
import javax.validation.constraints.NotNull

@Entity
data class Membro(

        @NotNull(message = "pessoa nao pode ser nula.")
        @ManyToOne(optional = false)
        @JoinColumn(name = "pessoa_idpessoa", nullable = false)
        val pessoa: Pessoa,

        @NotNull(message = "igreja nao pode ser nula.")
        @ManyToOne(optional = false)
        @JoinColumn(name = "igreja_idigreja", nullable = false)
        val igreja: Igreja,

        @NotNull(message = "tipo nao pode ser nulo.")
        @Enumerated(EnumType.STRING)
        @Column(nullable = false, columnDefinition = "char(10)")
        val tipo: Tipo = Tipo.MEMBRO,

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "idmembro")
        val idMembro: Long? = null
)