package br.com.felix.membroservice.membro

import br.com.felix.membroservice.core.crud.CrudController
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("membro")
class MembroController(private val service: MembroService) : CrudController<Membro, Long>() {

    override fun getService() = service

}