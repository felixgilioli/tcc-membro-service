package br.com.felix.membroservice.email

import br.com.felix.membroservice.core.crud.CrudService

interface EmailService : CrudService<Email, Long> {
}