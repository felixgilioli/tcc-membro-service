package br.com.felix.membroservice.email

import br.com.felix.membroservice.core.crud.CrudServiceImpl
import org.springframework.stereotype.Service

@Service
class EmailServiceImpl(private val repository: EmailRepository) : EmailService, CrudServiceImpl<Email, Long>() {

    override fun getRepository() = repository
}