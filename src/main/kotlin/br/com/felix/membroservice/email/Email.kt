package br.com.felix.membroservice.email

import br.com.felix.membroservice.pessoa.Pessoa
import javax.persistence.*
import javax.validation.constraints.Email

@Entity
data class Email(

        @Email(message = "email invalido.")
        @Column(length = 100, nullable = false)
        val email: String,

        @ManyToOne(optional = false)
        @JoinColumn(name = "pessoa_idpessoa", nullable = false)
        val pessoa: Pessoa,

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "idemail")
        val idEmail: Long? = null
)