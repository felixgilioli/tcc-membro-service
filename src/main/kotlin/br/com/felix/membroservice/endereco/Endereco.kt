package br.com.felix.membroservice.endereco

import br.com.felix.membroservice.pessoa.Pessoa
import org.hibernate.validator.constraints.Length
import javax.persistence.*

@Entity
data class Endereco(

        @Length(min = 2, max = 100,  message = "cidade deve estar entre 2 e 100 caracteres.")
        @Column(length = 100, nullable = false)
        val cidade: String,

        @Length(min = 2, max = 50,  message = "pais deve estar entre 2 e 100 caracteres.")
        @Column(length = 50, nullable = false)
        val pais: String,

        @Length(min = 2, max = 50,  message = "estado deve estar entre 2 e 50 caracteres.")
        @Column(length = 50, nullable = false)
        val estado: String,

        @Length(max = 100,  message = "bairro nao pode ultrapassar 100 caracteres.")
        @Column(length = 100)
        val bairro: String? = null,

        @Length(max = 10,  message = "cep nao pode ultrapassar 10 caracteres.")
        @Column(length = 10)
        val cep: String? = null,

        @Length(max = 100,  message = "rua nao pode ultrapassar 100 caracteres.")
        @Column(length = 100)
        val rua: String? = null,

        @Column
        val numero: Int? = null,

        @ManyToOne
        @JoinColumn(name = "pessoa_idpessoa")
        val pessoa: Pessoa? = null,

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "idendereco")
        val idEndereco: Long? = null
)