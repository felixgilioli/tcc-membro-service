package br.com.felix.membroservice.endereco

import br.com.felix.membroservice.core.crud.CrudService

interface EnderecoService : CrudService<Endereco, Long> {
}