package br.com.felix.membroservice.endereco

import br.com.felix.membroservice.core.crud.CrudServiceImpl
import org.springframework.stereotype.Service

@Service
class EnderecoServiceImpl(private val repository: EnderecoRepository) : EnderecoService, CrudServiceImpl<Endereco, Long>() {

    override fun getRepository() = repository
}