/*
 * This file is generated by jOOQ.
 */
package br.com.felix.membroservice.jooq.tables;


import br.com.felix.membroservice.jooq.Indexes;
import br.com.felix.membroservice.jooq.Keys;
import br.com.felix.membroservice.jooq.Membro;
import br.com.felix.membroservice.jooq.tables.records.PessoaRecord;

import java.sql.Date;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Identity;
import org.jooq.Index;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.9"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Pessoa extends TableImpl<PessoaRecord> {

    private static final long serialVersionUID = 1315666031;

    /**
     * The reference instance of <code>membro.pessoa</code>
     */
    public static final Pessoa PESSOA = new Pessoa();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<PessoaRecord> getRecordType() {
        return PessoaRecord.class;
    }

    /**
     * The column <code>membro.pessoa.idpessoa</code>.
     */
    public final TableField<PessoaRecord, Long> IDPESSOA = createField("idpessoa", org.jooq.impl.SQLDataType.BIGINT.nullable(false).identity(true), this, "");

    /**
     * The column <code>membro.pessoa.nome</code>.
     */
    public final TableField<PessoaRecord, String> NOME = createField("nome", org.jooq.impl.SQLDataType.VARCHAR(100).nullable(false), this, "");

    /**
     * The column <code>membro.pessoa.dob</code>.
     */
    public final TableField<PessoaRecord, Date> DOB = createField("dob", org.jooq.impl.SQLDataType.DATE.nullable(false), this, "");

    /**
     * The column <code>membro.pessoa.cpf</code>.
     */
    public final TableField<PessoaRecord, String> CPF = createField("cpf", org.jooq.impl.SQLDataType.VARCHAR(15), this, "");

    /**
     * The column <code>membro.pessoa.data_cadastro</code>.
     */
    public final TableField<PessoaRecord, Date> DATA_CADASTRO = createField("data_cadastro", org.jooq.impl.SQLDataType.DATE.nullable(false), this, "");

    /**
     * The column <code>membro.pessoa.genero</code>. M-MASCULINO
F-FEMININO
     */
    public final TableField<PessoaRecord, String> GENERO = createField("genero", org.jooq.impl.SQLDataType.CHAR(1).nullable(false), this, "M-MASCULINO\nF-FEMININO");

    /**
     * The column <code>membro.pessoa.status_civil</code>.
     */
    public final TableField<PessoaRecord, String> STATUS_CIVIL = createField("status_civil", org.jooq.impl.SQLDataType.CHAR(10), this, "");

    /**
     * The column <code>membro.pessoa.foto_dir</code>.
     */
    public final TableField<PessoaRecord, String> FOTO_DIR = createField("foto_dir", org.jooq.impl.SQLDataType.VARCHAR(255), this, "");

    /**
     * Create a <code>membro.pessoa</code> table reference
     */
    public Pessoa() {
        this(DSL.name("pessoa"), null);
    }

    /**
     * Create an aliased <code>membro.pessoa</code> table reference
     */
    public Pessoa(String alias) {
        this(DSL.name(alias), PESSOA);
    }

    /**
     * Create an aliased <code>membro.pessoa</code> table reference
     */
    public Pessoa(Name alias) {
        this(alias, PESSOA);
    }

    private Pessoa(Name alias, Table<PessoaRecord> aliased) {
        this(alias, aliased, null);
    }

    private Pessoa(Name alias, Table<PessoaRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""));
    }

    public <O extends Record> Pessoa(Table<O> child, ForeignKey<O, PessoaRecord> key) {
        super(child, key, PESSOA);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return Membro.MEMBRO;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.PESSOA_PRIMARY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Identity<PessoaRecord, Long> getIdentity() {
        return Keys.IDENTITY_PESSOA;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<PessoaRecord> getPrimaryKey() {
        return Keys.KEY_PESSOA_PRIMARY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<PessoaRecord>> getKeys() {
        return Arrays.<UniqueKey<PessoaRecord>>asList(Keys.KEY_PESSOA_PRIMARY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Pessoa as(String alias) {
        return new Pessoa(DSL.name(alias), this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Pessoa as(Name alias) {
        return new Pessoa(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public Pessoa rename(String name) {
        return new Pessoa(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public Pessoa rename(Name name) {
        return new Pessoa(name, null);
    }
}
