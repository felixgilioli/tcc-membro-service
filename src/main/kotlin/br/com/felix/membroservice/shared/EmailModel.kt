package br.com.felix.membroservice.shared

data class EmailModel(
        val para: Set<String>,
        val titulo: String,
        val mensagem: String
)