package br.com.felix.membroservice.pessoa.to

import javax.persistence.Column

data class PessoaSample(

        @Column(name = "idpessoa")
        var idPessoa: Long,

        var nome: String,

        var email: String
)