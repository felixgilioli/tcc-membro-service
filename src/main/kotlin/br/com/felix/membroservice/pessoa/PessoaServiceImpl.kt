package br.com.felix.membroservice.pessoa

import br.com.felix.membroservice.core.crud.CrudServiceImpl
import br.com.felix.membroservice.pessoa.to.PessoaSample
import br.com.felix.membroservice.shared.EmailModel
import com.google.gson.Gson
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import java.time.LocalDate

const val TOPIC_ANIVERSARIANTES = "aniversariantes"

@Service
class PessoaServiceImpl(private val repository: PessoaRepository,
                        private val kafkaTemplate: KafkaTemplate<String, String>,
                        private val gson: Gson)
    : PessoaService, CrudServiceImpl<Pessoa, Long>() {

    override fun getRepository() = repository

    override fun findAniversariantes() = repository.findAniversariantes(LocalDate.now())

    @Scheduled(cron = "0 0 0 * * ?") //segundo minuto hora
    fun getAniversariantes() {
        val aniversariantes = findAniversariantes()

        if (aniversariantes != null) {
            val email = EmailModel(aniversariantes.map(PessoaSample::email).toSet(),
                    "Parabéns pelo seu aniversário!",
                    "Parabéns, muitas felicidades.")

            kafkaTemplate.send(TOPIC_ANIVERSARIANTES, gson.toJson(email))
        }
    }

}