package br.com.felix.membroservice.pessoa

import br.com.felix.membroservice.pessoa.to.PessoaSample
import java.time.LocalDate

interface PessoaRepositoryCustom {

    fun findAniversariantes(dia: LocalDate) : List<PessoaSample>?

}