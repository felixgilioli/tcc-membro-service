package br.com.felix.membroservice.pessoa

import org.springframework.data.jpa.repository.JpaRepository

interface PessoaRepository : JpaRepository<Pessoa, Long>, PessoaRepositoryCustom {

}