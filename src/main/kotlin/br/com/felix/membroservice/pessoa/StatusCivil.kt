package br.com.felix.membroservice.pessoa

enum class StatusCivil {
    SOLTEIRO,
    CASADO,
    DIVORCIADO,
    VIUVO,
    SEPARADO
}