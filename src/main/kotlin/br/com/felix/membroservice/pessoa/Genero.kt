package br.com.felix.membroservice.pessoa

enum class Genero(val label: String) {
    M("Masculino"),
    F("Feminino")
}