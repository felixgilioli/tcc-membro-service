package br.com.felix.membroservice.pessoa

import br.com.felix.membroservice.core.crud.CrudService
import br.com.felix.membroservice.pessoa.to.PessoaSample

interface PessoaService : CrudService<Pessoa, Long> {

    fun findAniversariantes(): List<PessoaSample>?

}