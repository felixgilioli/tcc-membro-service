package br.com.felix.membroservice.pessoa

import br.com.felix.membroservice.jooq.tables.Email.EMAIL
import br.com.felix.membroservice.jooq.tables.Pessoa.PESSOA
import br.com.felix.membroservice.pessoa.to.PessoaSample
import org.jooq.DSLContext
import org.springframework.stereotype.Repository
import java.time.LocalDate

@Repository
class PessoaRepositoryImpl(private val dsl: DSLContext) : PessoaRepositoryCustom {

    override fun findAniversariantes(dia: LocalDate): List<PessoaSample>? = dsl.use {
        return it.selectDistinct(PESSOA.IDPESSOA, PESSOA.NOME, EMAIL.EMAIL_)
                .from(PESSOA)
                .innerJoin(EMAIL).on(EMAIL.PESSOA_IDPESSOA.eq(PESSOA.IDPESSOA))
                .fetchInto(PessoaSample::class.java)
    }

}