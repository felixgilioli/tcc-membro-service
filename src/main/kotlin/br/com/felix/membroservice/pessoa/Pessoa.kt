package br.com.felix.membroservice.pessoa

import org.hibernate.validator.constraints.Length
import org.hibernate.validator.constraints.br.CPF
import java.time.LocalDate
import javax.persistence.*
import javax.validation.constraints.NotNull

@Entity
data class Pessoa(

        @Length(min = 3, max = 100,  message = "nome deve estar entre 3 e 100 caracteres.")
        @Column(name = "nome", length = 100, nullable = false)
        val nome: String,

        @NotNull(message = "dob nao pode ser nula.")
        @Column(name = "dob", nullable = false)
        val dob: LocalDate,

        @CPF(message = "cpf invalido.")
        @Column(name = "cpf", length = 15)
        val cpf: String? = null,

        @NotNull(message = "dataCadastro nao pode ser nula.")
        @Column(name = "data_cadastro", nullable = false)
        val dataCadastro: LocalDate = LocalDate.now(),

        @NotNull(message = "genero nao pode ser nulo.")
        @Enumerated(EnumType.STRING)
        @Column(name = "genero", nullable = false, length = 1, columnDefinition = "char(1)")
        val genero: Genero,

        @Enumerated(EnumType.STRING)
        @Column(name = "status_civil", length = 10, columnDefinition = "char(10)")
        val statusCivil: StatusCivil? = null,

        @Length(max = 255, message = "fotoDir nao pode ultrapassar 255 caracteres.")
        @Column(name = "foto_dir", length = 255)
        val fotoDir: String? = null,

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "idpessoa")
        val idPessoa: Long? = null
)