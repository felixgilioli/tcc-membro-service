package br.com.felix.membroservice.pessoa

import br.com.felix.membroservice.core.crud.CrudController
import br.com.felix.membroservice.pessoa.to.PessoaSample
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("pessoa")
class PessoaController(private val service: PessoaService) : CrudController<Pessoa, Long>() {

    override fun getService() = service

    @GetMapping("aniversariantes")
    fun teste() : List<PessoaSample>? {
        return service.findAniversariantes()
    }
}