package br.com.felix.membroservice.core.config

import org.jooq.codegen.GenerationTool
import org.jooq.meta.jaxb.*
import org.jooq.meta.jaxb.Target
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.CommandLineRunner
import org.springframework.stereotype.Component

@Component
class JooqGenerator : CommandLineRunner {

    @Value("\${spring.datasource.url}")
    val datasourceUrl: String? = null

    @Value("\${spring.datasource.username}")
    val username: String? = null

    @Value("\${spring.datasource.password}")
    val password: String? = null

    @Value("\${spring.datasource.driverClassName}")
    val driverClassName: String? = null

    @Value("\${jooq.database.name}")
    val name: String? = null

    @Value("\${jooq.database.schema}")
    val schema: String? = null

    override fun run(vararg args: String?) {
        val configuration = Configuration()
                .withJdbc(Jdbc()
                        .withDriver(driverClassName)
                        .withUrl(datasourceUrl)
                        .withUser(username)
                        .withPassword(password))
                .withGenerator(Generator()
                        .withDatabase(Database()
                                .withName(name)
                                .withIncludes(".*")
                                .withExcludes("")
                                .withInputSchema(schema))
                        .withTarget(Target()
                                .withPackageName("br.com.felix.membroservice.jooq")
                                .withDirectory("src/main/kotlin")))
        GenerationTool.generate(configuration)
    }
}