package br.com.felix.membroservice

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.PropertySource
import org.springframework.test.context.junit4.SpringRunner

@PropertySource("classpath:application-integration.properties")
@RunWith(SpringRunner::class)
@SpringBootTest
class MembroServiceApplicationTests {

	@Test
	fun contextLoads() {
	}

}

